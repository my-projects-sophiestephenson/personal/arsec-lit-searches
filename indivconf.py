from scholarly import scholarly, ProxyGenerator
from pybliometrics.scopus import AbstractRetrieval
import time
from collections import defaultdict
import sys

keywords_vr = [
	"virtual reality", "augmented reality", "mixed reality", 
	"smart glasses", "head-mounted displays", "smartglasses", 
	"head-worn display"
]
keywords_auth = [
	"authentication", "user identification", "authenticate", "authenticating", 
	"authenticated", "password", "biometric"
]

def def_value():
	return 0


# generate tor proxy so scholar doesn't kick us off
pg = ProxyGenerator()
pg.Tor_Internal(tor_cmd = "tor")
scholarly.use_proxy(pg)
scholarly.set_retries(3)

pubs_accepted = 0
conf = sys.argv[1]
out = open(sys.argv[2], 'w')

auth_q = "authentication OR \"user identification\" OR authenticate OR authenticating OR authenticated OR password OR biometric source:\"{}\"".format(conf)
vr_q = '"virtual reality" OR "augmented reality" OR "mixed reality" OR "smart glasses" OR "head mounted displays" OR smartglasses OR "head worn display" source:\"{}\"'.format(conf)
paper_dict = defaultdict(def_value)	
ar_done = False

while (not ar_done):
	print("###############################################################################")
	print("NOW SEARCHING AUTH")
	print("###############################################################################")

	# reset paper dict
	paper_dict = defaultdict(def_value)	
	pubs_seen = 0	

	try:
		# do search
		print('.... AUTH PAPERS')
		search = scholarly.search_pubs(auth_q, patents=False, citations=False, year_low=2010)

		# go until getting the next query throws a StopIteration exception
		while (True):
			pubs_seen += 1
			if pubs_seen % 10 == 0:
				print("seen {} papers".format(pubs_seen))
			pub = next(search)
			paper_dict[pub['bib']['title']] = 1

	except Exception as e:
		if isinstance(e, StopIteration):
			ar_done = True
			print("all auth publications seen for this conference")
		else:
			print("EXCEPTION")
			print(e)


while (True):
	print("###############################################################################")
	print("NOW SEARCHING VR")
	print("###############################################################################")

	pubs_seen = 0
	papers_to_keep = []	

	try:
		# do vr search			
		print('.... VR PAPERS')
		search = scholarly.search_pubs(vr_q, patents=False, citations=False, year_low=2010)

		# go until getting the next query throws a StopIteration exception
		while (True):
			pubs_seen += 1
			if pubs_seen % 10 == 0:
				print("seen {} papers".format(pubs_seen))
			pub = next(search)
			bib = pub['bib']
			if paper_dict[bib['title']] == 0:
				continue
			
			s = bib['title'] + "\n(" + conf + ")\n"
			try:
				papers_to_keep.append(scholarly.bibtex(pub))
			except:
				papers_to_keep.append(s)
			print(s)
			pubs_accepted += 1

	except Exception as e:
		if isinstance(e, StopIteration):
			print("all vr publications seen for this conference")
			for p in papers_to_keep:
				out.write(p)
			break

		else:
			print("EXCEPTION")
			print(e)
			
out.close()
print(pubs_accepted, "publications accepted")