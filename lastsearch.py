# let this be the last time please for the love of god

from scholarly import scholarly, ProxyGenerator
from pybliometrics.scopus import AbstractRetrieval
import time
from collections import defaultdict

###############################################################################
########					lists of conferences 					   ########
###############################################################################


hci = [
	"Computer Human Interaction", 
	"Computer-Supported Cooperative Work & Social Computing", 	# CSCW
	"Conference on Pervasive and Ubiquitous Computing", # UbiComp
	"International Conference on Human Robot Interaction", # HRI
	"IEEE Transactions on Affective Computing",
	"Symposium on User Interface Software and Technology",
	"International Journal of Human-Computer Studies",  
	"IEEE Transactions on Human-Machine Systems", 
	"Behaviour & Information Technology", 
	"Transactions on Computer-Human Interaction", # TOCHI 
	"International Conference on Multimodal Interfaces", # ICMI
	"IEEE Transactions on Haptics", 
	"International Journal of Human-Computer Interaction",
	"Conference on Designing Interactive Systems", 
	"Universal Access in the Information Society", 
	"International Conference on Intelligent User Interfaces",
	"HCI International", 
	"Mobile HCI", 
	"IEEE Virtual Reality Conference", 
	"International Conference on Tangible, Embedded, and Embodied Interaction",
]

sec = [
	"ACM CCS", 
	"IEEE Transactions on Information Forensics and Security",
	"USENIX Security Symposium", 
	"IEEE Symposium on Security and Privacy",
	"Network and Distributed System Security Symposium", 
	"EUROCRYPT", 
	"Computers & Security", 
	"IEEE Transactions on Dependable and Secure Computing", 
	"International Cryptology Conference",
	"International Conference on Financial Cryptography and Data Security",
	"ASIACRYPT", 
	"Security and Communication Networks", 
	"Theory of Cryptography",
	"ACM on Asia Conference on Computer and Communications Security",
	"Proceedings on Privacy Enhancing Technologies", 
	"IEEE European Symposium on Security and Privacy",
	"Designs, Codes and Cryptography",
	"European Conference on Research in Computer Security",
	"IEEE Security & Privacy",
	"Journal of Information Security and Applications",
]

vision = [
	"Conference on Computer Vision and Pattern Recognition", 
	"IEEE International Conference on Computer Vision", 
	"European Conference on Computer Vision", 
	"IEEE Transactions on Pattern Analysis and Machine Intelligence",
	"IEEE Transactions on Image Processing", 
	"Pattern Recognition",
	"IEEE Computer Society Conference on Computer Vision and Pattern Recognition Workshops",
	"International Journal of Computer Vision", 
	"Medical Image Analysis",
	"Pattern Recognition Letters", 
	"British Machine Vision Conference",
	"Workshop on Applications of Computer Vision", 
	"IEEE International Conference on Image Processing",
	"International Conference on Computer Vision Workshops",
	"Computer Vision and Image Understanding",
	"Journal of Visual Communication and Image Representation",
	"IEEE International Conference on Automatic Face & Gesture Recognition",
	"International Conference on 3D Vision",
	"Image and Vision Computing",
	"International Conference on Pattern Recognition",
]

signal = [
	"IEEE Transactions on Image Processing", 
	"IEEE Transactions on Wireless Communications",
	"IEEE Transactions on Signal Processing",
	"International Conference on Acoustics, Speech and Signal Processing",
	"Conference of the International Speech Communication Association", 
	"Transactions on Circuits and Systems for Video Technology",
	"IEEE Signal Processing Letters",
	"Signal Processing",
	"IEEE Signal Processing Magazine",
	"Transactions on Audio, Speech, and Language Processing",
	"IEEE Journal of Selected Topics in Signal Processing",
	"IEEE Wireless Communications Letters",
	"IEEE International Conference on Image Processing",
	"IEEE Vehicular Technology Conference",
	"Journal of Visual Communication and Image Representation",
	"SIAM Journal on Imaging Sciences",
	"Digital Signal Processing",
	"Applied and Computational Harmonic Analysis",
	"Signal Processing: Image Communication",
	"IEEE Transactions on Computational Imaging",
]

general = [
	"Chemical engineering journal",
	"IEEE Transactions on Industrial Electronics",
	"IEEE Access",
	"Sensors",
	"Information Sciences",
	"ACS Energy Letters",
	"ACS Sustainable Chemistry & Engineering",
	"Fuel",
	"ACM Computing Surveys",
	"IEEE Transactions on Industrial Informatics",
	"Proceedings of the IEEE",
	"IEEE Transactions on Information Theory",
	"Procedia Computer Science",
	"International Journal of Information Management",
	"Mechanical Systems and Signal Processing",
	"Building and Environment",
	"Journal of Industrial and Engineering Chemistry",
	"IEEE Sensors Journal",
	"IEEE Transactions on Industry Applications",
	"IEEE Journal of Biomedical and Health Informatics",
]

# Categories: computer vision, security, mobile, hci
csrankings = [
	"Conference on Mobile Computing and Networking", "MobiCom",
	"Conference on Mobile Systems, Applications, and Services", "MobiSys", 
	"Conference on Embedded Networked Sensor Systems", "SenSys", 
	"Conference on Pervasive Computing and Communications", 
	"Interactive, Mobile, Wearable and Ubiquitous Technologies", "IMWUT",
]

megalist = list(set(hci + sec + vision + signal + general + csrankings))
megalist.sort()

redo_confs = [
	"Behaviour & Information Technology", 
	"British Machine Vision Conference",
	"Building and Environment", 
	"CHI", 
	"IEEE Access", 
	"CVPR", 
	"IEEE International Conference on Image Processing", 
	"IEEE Journal of Biomedical and Health Informatics", 
	"IEEE S&P", 
	"IEEE Signal Processing Magazine", 
	"IEEE Transactions on Affective Computing", 
	"IEEE Transactions on Human-Machine Systems", 
	"IEEE Transactions on Image Processing", 
	"IEEE Transactions on Industrial Electronics", 
	"IEEE Transactions on Pattern Analysis and Machine Intelligence",
	"IEEE Transactions on Signal Processing",
	"International Conference on Intelligent User Interfaces",
	"International Journal of Human-Computer Interaction",
	"International Journal of Human-Computer Studies",
	"Journal of Information Security and Applications",
	"Medical Image Analysis", 
	"MobiCom",
	"Pattern Recognition",
	"Pattern Recognition Letters",
	"Procedia Computer Science",
	"Proceedings of the IEEE",
	"Sensors",
	"Signal Processing",
	"Signal Processing: Image Communication",
	"Symposium on User Interface Software and Technology",
	"Transactions on Computer-Human Interaction",
	"USENIX Security Symposium", 
	"Universal Access in the Information Society",
	"Workshop on Applications of Computer Vision",
]

###############################################################################
########					functions & other variables				   ########
###############################################################################

keywords_vr = [
	"virtual reality", "augmented reality", "mixed reality", 
	"smart glasses", "head-mounted displays", "smartglasses", 
	"head-worn display"
]
keywords_auth = [
	"authentication", "user identification", "authenticate", "authenticating", 
	"authenticated", "password", "biometric"
]

def is_relevant(bib):
	title = bib['title'].lower()
	title_vr   = any(k in title for k in keywords_vr)
	title_auth = any(k in title for k in keywords_auth)

	try:
		ab = bib['abstract'].lower()
		abs_vr   = any(k in ab for k in keywords_vr)
		abs_auth = any(k in ab for k in keywords_auth)
		return (abs_vr and abs_auth) or (title_vr and title_auth)
	except:
		return title_vr and title_auth

def def_value():
	return 0

###############################################################################
########							search							   ########
###############################################################################

# generate tor proxy so scholar doesn't kick us off
pg = ProxyGenerator()
pg.Tor_Internal(tor_cmd = "tor")
scholarly.use_proxy(pg)
scholarly.set_retries(3)

#num_confs = len(megalist)
pubs_accepted = 0
total_pubs = 0
out = open('sensorsonly.txt', 'w')

redo_confs = ["Sensors"]

# go until every one has passed without erroring out
while len(redo_confs) > 0:
	num_confs = len(redo_confs)

	#for i, conf in enumerate(megalist, start=1):
	for i, conf in enumerate(redo_confs, start=1):
		auth_q = "authentication OR \"user identification\" OR authenticate OR authenticating OR authenticated OR password OR biometric source:\"{}\"".format(conf)
		vr_q = '"virtual reality" OR "augmented reality" OR "mixed reality" OR "smart glasses" OR "head mounted displays" OR smartglasses OR "head worn display" source:\"{}\"'.format(conf)
		pubs_seen = 0
		paper_dict = defaultdict(def_value)
		skip_vr = False
		
		print("###############################################################################")
		print("NOW SEARCHING: {}  (conference # {}/{})".format(conf, i, num_confs))
		print("###############################################################################")

		try:
			# do search
			print('.... AUTH PAPERS')
			search = scholarly.search_pubs(auth_q, patents=False, citations=False, year_low=2010)

			# go until getting the next query throws a StopIteration exception
			while (True):
				pubs_seen += 1
				if pubs_seen % 10 == 0:
					print("seen {} papers".format(pubs_seen))
				pub = next(search)
				paper_dict[pub['bib']['title']] = 1

		except Exception as e:
			total_pubs += pubs_seen
			if isinstance(e, StopIteration):
				print("all auth publications seen for this conference")
			else:
				print("EXCEPTION")
				print(e)
				continue

		pubs_seen = 0
		papers_to_keep = []

		try:
			# do vr search
			print('.... VR PAPERS')
			search = scholarly.search_pubs(vr_q, patents=False, citations=False, year_low=2010)

			# go until getting the next query throws a StopIteration exception
			while (True):
				pubs_seen += 1
				if pubs_seen % 10 == 0:
					print("seen {} papers".format(pubs_seen))
				pub = next(search)
				bib = pub['bib']
				if paper_dict[bib['title']] == 0:
					continue

				s = bib['title'] + "\n(" + conf + ")\n"
				try:
					papers_to_keep.append(scholarly.bibtex(pub))
				except:
					papers_to_keep.append(s)
				print(s)
				pubs_accepted += 1

		except Exception as e:
			total_pubs += pubs_seen
			if isinstance(e, StopIteration):
				print("all vr publications seen for this conference")
				for p in papers_to_keep:
					out.write(p)
				redo_confs.remove(conf)

			else:
				print("EXCEPTION")
				print(e)
			
		

out.close()
print(pubs_accepted, "publications accepted")
print(total_pubs, "publications seen in total")